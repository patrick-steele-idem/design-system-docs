module.exports = {
	onMount: function() {
		$('nav').slimScroll({
            height: '100vh'
        });
	},
	onInput: function(input) {
		var sections = ["Overview","Getting Started","Visual","Components","Javascript","Code Samples"];
		var pages = [
			{ name:"Overview", children:["Purpose","Releases","Requests"]},
			{ name:"Getting Started", children: ["Design Share"]},
			{ name:"Visual", children:["Animation","Colors","Grids","Iconography","Images","Logos","Spacing","Typography"] },
			{ name:"Components", children:["Accordion","Alert","Breadcrumb","Button","Card","Carousel","Flag","Form Fields","HR","Image Zoom","Menu",
																		 "Modals","Pagination","Panel","Progress Bar","Rating","Scroll to Top","Spinner","Table","Tabs","Tool Tips"] },
			{ name:"Javascript" },
			{ name:"Code Samples", children:["Header","Footer","Primary Navigation","Sub Nav","Product Card","Utilities"] }
		];
		
  	this.state = {
			sections: sections,
  			pages: pages
  	};
	},
	onClickParent: function(e,ele) {
		e.preventDefault();
		var parentPage = ele.text;
		$(this.state.pages).each(function(key,value){
			if(value.name == parentPage) {
				if(value.children !== undefined && value.children.length > 0) {
					$(ele).closest("li").find("ul").slideToggle();
					$(ele).closest("li").toggleClass('active');
				} else {
					console.log("Go to Page: " + $(ele).text());
				}
			}
		});
    }
};